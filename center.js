var source = new ol.source.Vector({
  url: 'data/geojson/france.json',
  format: new ol.format.GeoJSON()
});

var style = new ol.style.Style({
  fill: new ol.style.Fill({
    color: 'rgba(255, 255, 255, 0.6)'
  }),
  stroke: new ol.style.Stroke({
    color: '#319FD3',
    width: 1
  }),
  image: new ol.style.Circle({
    radius: 5,
    fill: new ol.style.Fill({
      color: 'rgba(255, 255, 255, 0.6)'
    }),
    stroke: new ol.style.Stroke({
      color: '#319FD3',
      width: 1
    })
  })
});

var vectorLayer = new ol.layer.Vector({
  source: source,
  style: style
});

var view = new ol.View({
  center: [0, 0],
  zoom: 1
});
var map = new ol.Map({
  layers: [
    new ol.layer.Tile({
      source: new ol.source.OSM()
    }),
    vectorLayer,

    new ol.layer.Image({
      source: new ol.source.ImageVector({
        source: new ol.source.Vector({
          url: 'data/geojson/france.json',
          format: new ol.format.GeoJSON()
        }),
        style: new ol.style.Style({
          fill: new ol.style.Fill({
            color: 'rgba(255, 255, 255, 0.6)'
          }),
          stroke: new ol.style.Stroke({
            color: '#319FD3',
            width: 1
          })
        })
      })
    })
  ],
  target: 'map',
  controls: ol.control.defaults({
    attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
      collapsible: false
    })
  }),
  view: view
});

var featureOverlay = new ol.layer.Vector({
  source: new ol.source.Vector(),
  map: map,
  style: new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: '#f00',
      width: 1
    }),
    fill: new ol.style.Fill({
      color: 'rgba(255,0,0,0.1)'
    })
  })
});

var zoomtofrancebest = document.getElementById('zoomtofrancebest');
zoomtofrancebest.addEventListener('click', function() {
  var feature = source.getFeatures()[0];
  var polygon = /** @type {ol.geom.SimpleGeometry} */ (feature.getGeometry());
  var size = /** @type {ol.Size} */ (map.getSize());
  view.fit(
      polygon,
      size,
      {
        padding: [170, 50, 30, 150],
        constrainResolution: false
      }
  );
}, false);

var zoomtoswitzerlandconstrained =
    document.getElementById('zoomtofranceconstrained');
zoomtoswitzerlandconstrained.addEventListener('click', function() {
  var feature = source.getFeatures()[0];
  var polygon = /** @type {ol.geom.SimpleGeometry} */ (feature.getGeometry());
  var size = /** @type {ol.Size} */ (map.getSize());
  view.fit(
      polygon,
      size,
      {
        padding: [170, 50, 30, 150]
      }
  );
}, false);

var zoomtoswitzerlandnearest =
    document.getElementById('zoomtoswitzerlandnearest');
zoomtoswitzerlandnearest.addEventListener('click', function() {
  var feature = source.getFeatures()[0];
  var polygon = /** @type {ol.geom.SimpleGeometry} */ (feature.getGeometry());
  var size = /** @type {ol.Size} */ (map.getSize());
  view.fit(
      polygon,
      size,
      {
        padding: [170, 50, 30, 150],
        nearest: true
      }
  );
}, false);

